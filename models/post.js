module.exports = function(sequelize, DataTypes) {
	var Post = sequelize.define('Post', {
		id: { type: DataTypes.INTEGER, primaryKey: true, autoIncrement: true },
		title: { type: DataTypes.STRING, allowNull: true },
		body: { type: DataTypes.TEXT, allowNull: false },
		date: { type: DataTypes.DATE, allowNull: false, defaultValue: DataTypes.NOW },
	});
	return Post;
}
