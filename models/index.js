"use strict";

var fs        = require("fs");
var path      = require("path");
var Sequelize = require("sequelize");
var env       = process.env.NODE_ENV || "development";

// Conf file
if (process.argv[2] == undefined){
	console.log("Config file not specified!");
}
var confFile = "../" +  process.argv[2];
var config = require(confFile).db;

var sequelize = new Sequelize(config.database, config.username, config.password);
var db        = {};

fs.readdirSync(__dirname)
	.filter(function(file) {
		return (file.indexOf(".") !== 0) && (file !== "index.js");
	})
  .forEach(function(file) {
		var model = sequelize["import"](path.join(__dirname, file));
		db[model.name] = model;
	});

Object.keys(db).forEach(function(modelName) {
	  if ("associate" in db[modelName]) {
			    db[modelName].associate(db);
					  }
});

db.sequelize = sequelize;
db.Sequelize = Sequelize;

module.exports = db;
