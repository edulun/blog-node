var express = require('express');
var Post = require('../models').Post;
var router = express.Router();

router.get('/', function(req, res) {
	Post.find({
		where: {
			id: 1
		}
	}).then(function (post) {
		res.render('post', {'Post': post });
	});
});

/* GET home page. */
router.get('/:id', function(req, res) {
	//
	Post.find(req.id).then(function (post) {
		res.render('post', {'Post': post });
	});
});

module.exports = router;
